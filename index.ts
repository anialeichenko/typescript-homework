import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './src/styles/styles.css';
import { init } from './src/index';

init();
