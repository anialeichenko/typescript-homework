interface IMovie {
    id: string,
    title: string,
    poster_path: string,
    overview: string,
    backdrop_path: string,
    release_date: string,
}
interface IMovieFull extends IMovie {
    [key: string]: any,
}

const apiKey =  'dc70e6a78f248cb0dd69337d08c86237';

const moviesInfo = new Map();

let movies = {
    page: 1,
    results: [],
    totalPages: 1,
};

const resetMovies = () => {
    movies = {
        page: 1,
        results: [],
        totalPages: 1,
    };
}

const filmContainerElement = document.querySelector<HTMLElement>('#film-container');
const checkedRadioButton = document.querySelector<HTMLInputElement>('[name=btnradio]:checked');
const search = document.querySelector<HTMLInputElement>('#search');
const searchForm = document.querySelector<HTMLElement>('#search-form');
const loadMoreButton = document.querySelector<HTMLElement>('#load-more');
const randomMovieDescription = document.querySelector<HTMLElement>('#random-movie-description');
const randomMovieName = document.querySelector<HTMLElement>('#random-movie-name');
const randomMovie = document.querySelector<HTMLElement>('#random-movie');
const likedMoviesElement = document.querySelector<HTMLElement>('#favorite-movies');

let tag: string = checkedRadioButton?.value || '';
let randomItemId = '';

const likeFilmsArray = JSON.parse(localStorage.getItem("likeFilms") || '[]');
const likeFilmsSet = new Set<string>(likeFilmsArray);

document.querySelectorAll<HTMLInputElement>("[name=btnradio]").forEach((input) => {
    input.addEventListener('change', async (event) => {
        if (search) {
            search.value = '';
        }
        const { value } = event.target as HTMLInputElement;
        tag = value;
        resetMovies();
        await fetchData();
        render();
    });
});

searchForm?.addEventListener('submit', async (event) => {
    event.preventDefault();
    resetMovies();
    await fetchData();
    render()
});

loadMoreButton?.addEventListener('click', async () => {
    movies.page += 1;
    await fetchData();
    render();
});

const clickLikeHandler = (event: Event) => {
    const target = event.target as HTMLElement; 
    const element: HTMLElement = target.closest(".likeMovie") as HTMLElement;
    if(!element) {
        return;
    }

    const id = String(element.dataset.id);

    if (likeFilmsSet.has(id)) {
        likeFilmsSet.delete(id);
    } else {
        likeFilmsSet.add(id);
    }

    localStorage.setItem("likeFilms", JSON.stringify(likeFilmsSet));
    render();
    renderLikedMovies();
};

likedMoviesElement?.addEventListener('click', clickLikeHandler);
filmContainerElement?.addEventListener('click', clickLikeHandler);

interface IRequestURLs {
    [key: string]: any 
} 
const requestURLs: IRequestURLs = {
    byId: ({ id }: { id: string }) => `https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}&language=en-US`,
    popular: ({ page }: { page: number }) => `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&page=${page}`,
    upcoming: ({ page }: { page: number }) => `https://api.themoviedb.org/3/movie/upcoming?api_key=${apiKey}&page=${page}`,
    toprated: ({ page }: { page: number }) => `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&page=${page}`,
    search: ({ page, query }: { page: number, query: string }) => `https://api.themoviedb.org/3/search/movie?api_key=dc70e6a78f248cb0dd69337d08c86237&language=en-US&query=${query}&page=${page}&include_adult=false`,
};

const normalizeMovieInfo = (movie: IMovieFull): IMovie => ({
    id: String(movie.id),
    title: movie.title,
    poster_path: movie.poster_path,
    overview: movie.overview,
    backdrop_path: movie.backdrop_path,
    release_date: movie.release_date,
});

const fetchData = async () => {
    let url = '';
    if (search?.value) {
        url = requestURLs.search({ query: search.value, page: movies.page });
    } else {
        if (tag === '') {
            tag = 'popular';
        }
        url = requestURLs[tag]({ page: movies.page });
    }

    return fetch(url)
        .then(response => response.json())
        .then((result: {
            results: IMovieFull,
            total_pages: number,
        }) => {
            const ids = result.results.map((movie: IMovieFull): string => {
                moviesInfo.set(String(movie.id), normalizeMovieInfo(movie));
                return String(movie.id);
            })
            movies.results = movies.results.concat(ids);
            movies.totalPages = result.total_pages;
        });
}

export async function init(): Promise<void> {
    await fetchData();
    for (const id of Array.from(likeFilmsSet.values())) {
        await fetch(requestURLs.byId({ id }))
            .then(response => response.json())
            .then(movie => {
                moviesInfo.set(String(movie.id), normalizeMovieInfo(movie));
            });
    }
    randomItemId = movies.results[Math.floor(Math.random() * movies.results.length)];
    renderRandomMovie();
    renderLikedMovies();
    render();
}

export async function renderRandomMovie(): Promise<void> {
    if (randomItemId) {
        const item = moviesInfo.get(String(randomItemId));
        if (randomMovieDescription && randomMovieName && randomMovie) {
            randomMovieDescription.innerHTML = item.overview;
            randomMovieName.innerHTML = item.title;
            randomMovie.style.backgroundImage = `url(https://image.tmdb.org/t/p/original${item.backdrop_path})`;
        }
    }
}

export async function renderLikedMovies(): Promise<void> {
    if (likedMoviesElement) {
        likedMoviesElement.innerHTML = Array.from(likeFilmsSet.values()).map(id => {
            const str = Movie({ 
                movie: moviesInfo.get(id),
                liked: true,
            })
            return `<div class="col-12 p-2">${str}</div>`;
        }).join('');
    }
}

export async function render(): Promise<void> {
    if (search?.value) {
        const checkedRadio = document.querySelector<HTMLInputElement>('[name=btnradio]:checked')
        if(checkedRadio) {
            checkedRadio.checked = false;
        }
    } else {
        if (tag === '') {
            const popularRadio = document.querySelector<HTMLInputElement>('[name=btnradio][value=popular]');
            if (popularRadio) {
                popularRadio.checked  = true;
            }
        }
    }
    
    if (loadMoreButton) {
        loadMoreButton.hidden = movies.page >= movies.totalPages;
    }
    
    if (filmContainerElement) {
        filmContainerElement.innerHTML = movies.results.map(id => {
            const str = Movie({ 
                movie: moviesInfo.get(String(id)), 
                liked: likeFilmsSet.has(String(id)),
            });
            return `<div class="col-lg-3 col-md-4 col-12 p-2">${str}</div>`;
        }).join('');
    }
    console.log(moviesInfo);
    console.log(movies);

};

interface IMovieProps {
    movie: IMovie, 
    liked: boolean,
}
function Movie({ movie, liked = false }: IMovieProps) {
    return `
        <div class="card shadow-sm">
            <img
                src="https://image.tmdb.org/t/p/original/${movie.poster_path}"
            />
            <div class="bi bi-heart-fill position-absolute p-2 likeMovie" data-id="${movie.id}">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    stroke="red"
                    fill="${liked ? 'red': 'none'}"
                    width="50"
                    height="50"
                    class=""
                    viewBox="0 -2 18 22"
                
                >
                    <path
                        fill-rule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                    />
                </svg>
            </div>
            <div class="card-body">
                <p class="card-text truncate">${movie.overview}</p>
                <div
                    class="
                        d-flex
                        justify-content-between
                        align-items-center
                    "
                >
                    <small class="text-muted">${movie.release_date}</small>
                </div>
            </div>
        </div>
    `;
};